<!DOCTYPE html>
<!--[if IE 8]>
		<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" lang="en-US">
	<![endif]-->
<!--[if !(IE 8) ]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Log In &lsaquo; webelitex &#8212; WordPress</title>
<script>if (document.location.protocol != "https:") {document.location = document.URL.replace(/^http:/i, "https:");}</script><link rel='stylesheet' id='dashicons-css' href='/tbl/wp-includes/css/dashicons.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='buttons-css' href='/tbl/wp-includes/css/buttons.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='forms-css' href='/tbl/wp-admin/css/forms.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='l10n-css' href='/tbl/wp-admin/css/l10n.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='login-css' href='/tbl/wp-admin/css/login.min.css' type='text/css' media='all' />
<meta name='robots' content='noindex,noarchive' />
<meta name='referrer' content='strict-origin-when-cross-origin' />
<meta name="viewport" content="width=device-width" />
</head>
<body class="login login-action-login wp-core-ui  locale-en-us">
<div id="login">
<h1><a href="https://wordpress.org/" title="Powered by WordPress" tabindex="-1">Powered by WordPress</a></h1>
<form name="loginform" id="loginform" action="/tbl/wp-login.php" method="post">
<p>
<label for="user_login">Username or Email Address<br />
<input type="text" name="log" id="user_login" class="input" value="" size="20" /></label>
</p>
<p>
<label for="user_pass">Password<br />
<input type="password" name="pwd" id="user_pass" class="input" value="" size="20" /></label>
</p>
<p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label></p>
<p class="submit">
<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Log In" />
<input type="hidden" name="redirect_to" value="/wp-admin/" />
<input type="hidden" name="testcookie" value="1" />
</p>
</form>
<p id="nav">
<a href="/tbl/wp-login.php?action=lostpassword">Lost your password?</a>
</p>
<script type="text/javascript">
function wp_attempt_focus(){
setTimeout( function(){ try{
d = document.getElementById('user_login');
d.focus();
d.select();
} catch(e){}
}, 200);
}

wp_attempt_focus();
if(typeof wpOnload=='function')wpOnload();
</script>
<p id="backtoblog"><a href="/tbl/">&larr; Back to webelitex</a></p>
</div>
<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
<script type="text/javascript">/*<![CDATA[*/if ( !window.TL_Const ) {var TL_Const={"security":"058e9bc1e5","ajax_url":"\/wp-admin\/admin-ajax.php","action_conversion":"tve_leads_ajax_conversion","action_impression":"tve_leads_ajax_impression","custom_post_data":{"http_referrer":"\/wp-login.php"},"current_screen":{"screen_type":7,"screen_id":0},"ignored_fields":["email","_captcha_size","_captcha_theme","_captcha_type","_submit_option","_use_captcha","g-recaptcha-response","__tcb_lg_fc","__tcb_lg_msg","_state","_form_type","_error_message_option","_back_url","_submit_option","url","_asset_group","_asset_option","mailchimp_optin"]};} else {ThriveGlobal.$j.extend(true, TL_Const, {"security":"058e9bc1e5","ajax_url":"\/wp-admin\/admin-ajax.php","action_conversion":"tve_leads_ajax_conversion","action_impression":"tve_leads_ajax_impression","custom_post_data":{"http_referrer":"\/wp-login.php"},"current_screen":{"screen_type":7,"screen_id":0},"ignored_fields":["email","_captcha_size","_captcha_theme","_captcha_type","_submit_option","_use_captcha","g-recaptcha-response","__tcb_lg_fc","__tcb_lg_msg","_state","_form_type","_error_message_option","_back_url","_submit_option","url","_asset_group","_asset_option","mailchimp_optin"]})} /*]]> */</script> <div class="clear"></div>
</body>
</html>
