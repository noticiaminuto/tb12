<?xml version="1.0" encoding="UTF-8"?><rsd version="1.0" xmlns="http://archipelago.phrasewise.com/rsd">
  <service>
    <engineName>WordPress</engineName>
    <engineLink>https://wordpress.org/</engineLink>
    <homePageLink>http://guiadoestudo.acessovip.net.br</homePageLink>
    <apis>
      <api name="WordPress" blogID="1" preferred="true" apiLink="http://guiadoestudo.acessovip.net.br/xmlrpc.php" />
      <api name="Movable Type" blogID="1" preferred="false" apiLink="http://guiadoestudo.acessovip.net.br/xmlrpc.php" />
      <api name="MetaWeblog" blogID="1" preferred="false" apiLink="http://guiadoestudo.acessovip.net.br/xmlrpc.php" />
      <api name="Blogger" blogID="1" preferred="false" apiLink="http://guiadoestudo.acessovip.net.br/xmlrpc.php" />
      	<api name="WP-API" blogID="1" preferred="false" apiLink="http://guiadoestudo.acessovip.net.br/wp-json/" />
	    </apis>
  </service>
</rsd>
