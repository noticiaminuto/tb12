jQuery(document).ready(function($){

	jQuery.noConflict();

	// nice scroll
 	$('body').niceScroll({
		horizrailenabled: false,
		cursorborder: '',
		background: '',
		cursorcolor: 'rgba(0,0,0,.8)',
		cursorwidth: '8px',
		cursorborderradius: '6px',
		boxzoom: true,
		autohidemode: false,
		zindex: 999,
		scrollspeed: 92
	});

	// exit - popup
	ddexitpop.init({
		contentsource: ['id', 'exit-popup'],
		fxclass: false,
		// hideaftershow: false,
		displayfreq: 'always',
		mobileshowafter: 8000,
		onddexitpop: function($popup){
			$('#exit-popup').addClass('active');
		}
	});
	$('#exit-popup').on('click',function(){
		$(this).removeClass('active');
	});

	var triggered = false;
	$('body').on('mouseleave',function(e){
		if(e.offsetY - $(window).scrollTop() < 0 && !triggered){
			triggered = true;
			$('#exit-popup').addClass('active');
		}
	});

	/* $.exitIntent('enable');
	$(document).bind('exitintent',function(){
		// if( $.cookie('exit-popup') != 'closed' ){
			$('#exit-popup').addClass('active');
			// $.cookie('exit-popup', 'closed', { expires: 1 });
		// }
	});*/

	// btn - whatsapp
	$('#btn-whatsapp .btn-close').on('click',function(){
		$(this).toggleClass('disabled');
		$('#btn-whatsapp').toggleClass('disabled');
	});

	// scroll - mobile
	$('#block-9 .scroll').slick({
		autoplay: true,
		autoplaySpeed: 8000,
		fade: true,
		cssEase: 'linear'
	});

	// stock - counter
	stock = Math.floor(Math.random() * (97 - 77 + 1)) + 77;
	kit_um = 2;
	kit_dois = 4;
	kit_tres = 6;
	$('.stock .count').html(stock);
	function verify_stock(){
		stock_verify = $('#current').text();
		if ( stock_verify < 0 ) {
			$('.stock .count').html(0);
		}
	}

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_tres);
		verify_stock();
	}, 1000 * 2);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_tres);
		verify_stock();
	}, 1000 * 13);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_dois * 2);
		verify_stock();
	}, 1000 * 25);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_tres);
		verify_stock();
	}, 1000 * 36);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_um * 3);
		verify_stock();
	}, 1000 * 47);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_dois * 2);
		verify_stock();
	}, 1000 * 59);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_dois);
		verify_stock();
	}, 1000 * 66);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_tres * 3);
		verify_stock();
	}, 1000 * 77);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_dois * 2);
		verify_stock();
	}, 1000 * 87);

	setTimeout(function(){
		stock = $('.stock .count').text();
		$('.stock .count').html(stock - kit_dois);
		verify_stock();
	}, 1000 * 99);

	// counter - timer
	$('#discount .counter, #block-1 .div-counter .counter').countdowntimer({
		hours: 0,
		minutes: 10,
		seconds: 0,
		displayFormat: 'MS',
		labelsFormat: true
	});
	$('#block-3 .counter .numbers').countdowntimer({
		hours: 0,
		minutes: 10,
		seconds: 0,
		displayFormat: 'HMS',
		labelsFormat: true
	});

});